import gpxpy.gpx
import geopy.distance
import glob

TRACKS_FILEPATH = r"..\datas\traces\**\*.gpx.gz"
tracks = glob.glob(TRACKS_FILEPATH)


def get_gpx_stats(gpx_file):
    with open(gpx_file, 'r') as file:
        gpx = gpxpy.parse(file)

    duration = gpx.get_moving_data().moving_time
    distance = gpx.get_moving_data().moving_distance
    speed = 3.6 * distance / duration  # Converting speed from m/s to km/h

    delay = gpx.get_moving_data(
        stopped_speed_threshold=1).stopped_time  # Slower speed than 1 m/s is considered as stop.
    delay_factor = delay / (100 * distance)  # delay factor in s/100m

    first_point = gpx.tracks[0].segments[0].points[0].latitude, gpx.tracks[0].segments[0].points[0].longitude
    last_point = gpx.tracks[0].segments[0].points[-1].latitude, gpx.tracks[0].segments[0].points[-1].longitude
    direct_distance = geopy.distance.geodesic(first_point, last_point).km
    detour_factor = gpx.get_moving_data().moving_distance / direct_distance

    return {'speed': speed, 'delay_factor': delay_factor, 'detour_factor': detour_factor}
